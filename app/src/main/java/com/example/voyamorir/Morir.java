package com.example.voyamorir;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Morir extends formulario {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morir);
        textView = findViewById(R.id.textView);
        Pantalla pantalla = (Pantalla) getIntent().getSerializableExtra("object");
        textView.setText (pantalla.nombre+"\n"+pantalla.edat+"\n"+pantalla.genero+"\n"+pantalla.radio);
    }

}
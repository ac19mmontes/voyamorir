package com.example.voyamorir;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Calendar;

public class formulario extends AppCompatActivity {

    private static final String TAG = "formulario";
    private EditText nom,edad;
    private TextView calendar;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    String radio="";
    RadioGroup radioGroup;
    RadioButton button1,button2;
    private Pantalla muerte;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        nom=findViewById(R.id.editTextTextPersonName);
        edad=findViewById(R.id.editTextTextPersonEdad);
        calendar = (TextView) findViewById(R.id.calen);
        radioGroup=findViewById(R.id.radioGroup);
        button1=findViewById(R.id.radio_pirates);
        button2=findViewById(R.id.radio_ninjas);
        Button button = findViewById(R.id.button);
        spinner=findViewById(R.id.spinner);

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        formulario.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = month + "/" + day + "/" + year;
                calendar.setText(date);
            }
        };
    }
    public void onClick(View view) {

                if (button1.isChecked()==true){
                    radio=button1.getText().toString();
                }

                if (button2.isChecked()==true) {
                    radio =button2.getText().toString();
                }
        String nombre = nom.getText().toString();
        String edat =edad.getText().toString();
        String genero=spinner.toString();

        muerte = new Pantalla(nombre,edat,radio,genero);
        Intent intent = new Intent (this, Morir.class);
        intent.putExtra("object", muerte);
        startActivity(intent);
        }
    }
